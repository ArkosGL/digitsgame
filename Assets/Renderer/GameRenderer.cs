﻿using UnityEngine;
using UnityEngine.UI;

using Digits.Source;


namespace Digits.Renderer
{
	public class GameRenderer : MonoBehaviour
	{
		public static Colors Colors;
		public static GraphicRaycaster Raycaster;

		private static Canvas _canvas;
		private static Text _scoreText;
		private static RectTransform _fieldRoot;
		private static RectTransform _deckRoot;
		private static GameObject _cellPrefab;

		private static CellRenderer[,] Cells;
		private static CellRenderer[] Deck;


		public static void Init(Colors colors, Canvas canvas, GraphicRaycaster raycaster, Text scoreText, RectTransform fieldRoot, RectTransform deckRoot, GameObject cellPrefab)
		{
			Colors = colors;
			_canvas = canvas;
			Raycaster = raycaster;
			_scoreText = scoreText;
			_fieldRoot = fieldRoot;
			_deckRoot = deckRoot;
			_cellPrefab = cellPrefab;

			FieldInit();
			DeckInit();
		}


		private static void FieldInit()
		{
			var cells = GameController.Cells;
			var sizeX = cells.GetLength(0);
			var sizeY = cells.GetLength(1);

			_fieldRoot.sizeDelta = new Vector2(sizeX * Constants.CELL_SIZE, sizeY * Constants.CELL_SIZE);
			_fieldRoot.pivot = Vector2.zero;
			_fieldRoot.anchoredPosition -= _fieldRoot.sizeDelta / 2f;

			FieldUpdate();
		}


		public static void FieldUpdate()
		{
			var cells = GameController.Cells;
			var sizeX = cells.GetLength(0);
			var sizeY = cells.GetLength(1);

			if (Cells == null || cells.Length != cells.Length)
				Cells = new CellRenderer[sizeX, sizeY];

			var score = 0;

			var possibleMoves = false;

			for (int x = 0; x < sizeX; x++)
			{
				for (int y = 0; y < sizeY; y++)
				{
					var cr = Cells[x, y];
					if (cr == null)
					{
						var go = Instantiate(_cellPrefab, _fieldRoot);
						cr = Cells[x, y] = go.GetComponent<CellRenderer>();
					}

					var cell = cells[x, y];
					cr.Init(cell, x, y, false);
					score += cell.Power;
					if (cell.Type == CellType.Empty)
						possibleMoves = true;
				}
			}

			_scoreText.text = score.ToString();
			var best = 0;
			if (PlayerPrefs.HasKey(Constants.BEST_SCORE))
			{
				best = PlayerPrefs.GetInt(Constants.BEST_SCORE);
				_scoreText.text += "\nBest: " + best;
			}


			if (!possibleMoves)
			{
				if (score > best)
					PlayerPrefs.SetInt(Constants.BEST_SCORE, score);

				GameController.Init();
				FieldUpdate();
				DeckUpdate();
			}
		}


		private static void DeckInit()
		{
			var deck = GameController.Deck;
			var deckSize = deck.Length;

			_deckRoot.sizeDelta = new Vector2(deckSize * Constants.CELL_SIZE, Constants.CELL_SIZE);
			_deckRoot.pivot = Vector2.zero;
			_deckRoot.anchoredPosition = new Vector2(0f, -_fieldRoot.sizeDelta.y - Constants.CELL_SIZE * 1.5f) / 2f;
			_deckRoot.anchoredPosition -= _deckRoot.sizeDelta / 2f;

			DeckUpdate();
		}


		private static void DeckUpdate()
		{
			var deck = GameController.Deck;
			var deckSize = deck.Length;

			if (Deck == null || Deck.Length != deck.Length)
				Deck = new CellRenderer[deckSize];

			for (int x = 0; x < deckSize; x++)
			{
				var cr = Deck[x];
				if (cr == null)
				{
					var cell = Instantiate(_cellPrefab, _deckRoot);
					cr = Deck[x] = cell.GetComponent<CellRenderer>();
				}
				cr.Init(deck[x], x, 0, true);
			}
		}
	}
}
