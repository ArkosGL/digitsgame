﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Digits.Source;


namespace Digits.Renderer
{
	[ExecuteInEditMode]
	public class CellWalls : MonoBehaviour
	{
		[SerializeField]
		private Transform _wallsRoot;
		[SerializeField]
		private PairWrap[] _walls;


#if UNITY_EDITOR
		private void Start()
		{
			_walls = new PairWrap[_wallsRoot.childCount];
			for (CellSide side = CellSide.none + 1; side < CellSide.count; side++)
			{
				_walls[(int)side - 1] = new PairWrap() { Side = side, Image = _wallsRoot.GetChild((int)side - 1).gameObject };
			}
		}
#endif


		public void SetWalls(HashSet<CellSide> walls)
		{
			for (CellSide side = CellSide.none + 1; side < CellSide.count; side++)
			{
				_walls.FirstOrDefault(p => p.Side == side).Image.SetActive(walls.Contains(side));
			}
		}


		[Serializable]
		private class PairWrap
		{
			public CellSide Side;
			public GameObject Image;
		}
	}
}
