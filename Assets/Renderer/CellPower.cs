﻿using UnityEngine;
using UnityEngine.UI;


namespace Digits.Renderer
{
	[ExecuteInEditMode]
	public class CellPower : MonoBehaviour
	{
		[SerializeField]
		private Text _text;


#if UNITY_EDITOR
		private void Start()
		{
			if (_text == null)
				_text = GetComponentInChildren<Text>(true);
		}
#endif


		public void SetPower(int power)
		{
			if (power > 100)
				_text.fontSize = 42;
			else
			if (power > 10)
				_text.fontSize = 64;
			else
				_text.fontSize = 75;

			_text.text = power.ToString();
		}
	}
}
