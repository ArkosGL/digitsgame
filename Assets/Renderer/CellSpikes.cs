﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Digits.Source;


namespace Digits.Renderer
{
	[ExecuteInEditMode]
	public class CellSpikes : MonoBehaviour
	{
		[SerializeField]
		private Transform _spikesRoot;
		[SerializeField]
		private PairWrap[] _spikes;


#if UNITY_EDITOR
		private void Start()
		{
			_spikes = new PairWrap[_spikesRoot.childCount];
			for (CellSide side = CellSide.none + 1; side < CellSide.count; side++)
			{
				_spikes[(int)side - 1] = new PairWrap() { Side = side, Image = _spikesRoot.GetChild((int)side - 1).gameObject };
			}
		}
#endif


		public void SetSpikes(HashSet<CellSide> spikes)
		{
			for (CellSide side = CellSide.none + 1; side < CellSide.count; side++)
			{
				_spikes.FirstOrDefault(p => p.Side == side).Image.SetActive(spikes.Contains(side));
			}
		}


		[Serializable]
		private class PairWrap
		{
			public CellSide Side;
			public GameObject Image;
		}
	}
}
