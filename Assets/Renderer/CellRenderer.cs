﻿using System.Collections.Generic;
using UnityEngine;

using Digits.Source;


namespace Digits.Renderer
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(CellColoration), typeof(CellPositioning), typeof(CellPower))]
	[RequireComponent(typeof(CellWalls), typeof(CellSpikes), typeof(CellDrag))]
	public class CellRenderer : MonoBehaviour
	{
		[SerializeField]
		private CellPositioning _position;
		[SerializeField]
		private CellColoration _color;
		[SerializeField]
		private CellPower _power;
		[SerializeField]
		private CellWalls _walls;
		[SerializeField]
		private CellSpikes _spikes;
		[SerializeField]
		private CellDrag _drag;

		private Cell _cell;

		public bool Replaceable { get; private set; }


#if UNITY_EDITOR
		private void Start()
		{
			if (_position == null)
				_position = GetComponent<CellPositioning>();
			if (_color == null)
				_color = GetComponent<CellColoration>();
			if (_power == null)
				_power = GetComponent<CellPower>();
			if (_walls == null)
				_walls = GetComponent<CellWalls>();
			if (_spikes == null)
				_spikes = GetComponent<CellSpikes>();
			if (_drag == null)
				_drag = GetComponent<CellDrag>();
		}
#endif


		public void Init(Cell cell, int x, int y, bool draggable)
		{
			_cell = cell;

			_color.SetType(_cell.Type);
			_drag.enabled = draggable;
			Replaceable = _cell.Type == CellType.Empty;
			_position.SetPosition(x, y);
			_power.SetPower(_cell.Power);
			_walls.SetWalls(_cell.Walls);
			_spikes.SetSpikes(draggable ? _cell.Spikes : new HashSet<CellSide>());

			name = "Cell:" + x + ":" + y + ":" + _cell.Type;
			if (_cell.Type != CellType.Empty && _cell.Type != CellType.Hypno)
				name += ":" + _cell.Power;
		}
	}
}
