﻿using UnityEngine;
using UnityEngine.UI;

using Digits.Source;


namespace Digits.Renderer
{
	[ExecuteInEditMode]
	public class GameStarter : MonoBehaviour
	{
		[SerializeField]
		private Colors _colors;

		[SerializeField]
		private Canvas _canvas;
		[SerializeField]
		private GraphicRaycaster _raycaster;
		[SerializeField]
		private Text _scoreText;
		[SerializeField]
		private RectTransform _fieldRoot;
		[SerializeField]
		private RectTransform _deckRoot;
		[SerializeField]
		private GameObject _cellPrefab;


		private void Start()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				_canvas = FindObjectOfType<Canvas>();
				_raycaster = FindObjectOfType<GraphicRaycaster>();
			}
			else
			{
#endif
				GameController.Init();
				GameRenderer.Init(_colors, _canvas, _raycaster, _scoreText, _fieldRoot, _deckRoot, _cellPrefab);
#if UNITY_EDITOR
			}
#endif
		}
	}
}
