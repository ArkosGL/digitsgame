﻿using UnityEngine;
using UnityEngine.UI;

using Digits.Source;


namespace Digits.Renderer
{
	[ExecuteInEditMode]
	public class CellColoration : MonoBehaviour
	{
		[SerializeField]
		private GameObject _empty;
		[SerializeField]
		private GameObject _hypno;
		[SerializeField]
		private GameObject _filled;
#if UNITY_EDITOR
		[SerializeField]
		private GameObject _spikesRoot;
#endif
		[SerializeField]
		private Image[] _imagesToColorate;


#if UNITY_EDITOR
		private void Start()
		{
			var spikes = _spikesRoot.GetComponentsInChildren<Image>(true);
			_imagesToColorate = new Image[spikes.Length + 1];

			_imagesToColorate[0] = _filled.GetComponent<Image>();
			for (int i = 0; i < spikes.Length; i++)
			{
				_imagesToColorate[i + 1] = spikes[i];
			}
		}
#endif


		public void SetType(CellType type)
		{
			var empty = type == CellType.Empty;
			var hypno = type == CellType.Hypno;
			var fill = !empty && !hypno;

			_empty.SetActive(empty);
			_hypno.SetActive(hypno);
			_filled.SetActive(fill);

			if (fill)
			{
				var color = GameRenderer.Colors.Color(type);

				foreach (var image in _imagesToColorate)
				{
					image.color = color;
				}
			}
		}
	}
}
