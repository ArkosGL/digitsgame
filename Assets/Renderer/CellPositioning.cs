﻿using UnityEngine;


namespace Digits.Renderer
{
	[ExecuteInEditMode]
	public class CellPositioning : MonoBehaviour
	{
		[SerializeField]
		private Transform _tf;

		public int X;
		public int Y;
		public Vector2 Position { get; private set; }


#if UNITY_EDITOR
		private void Start()
		{
			if (_tf == null)
				_tf = transform;
		}
#endif


		public void SetPosition(int x, int y)
		{
			X = x;
			Y = y;
			_tf.localPosition = Position = new Vector2(x + .5f, y + .5f) * Constants.CELL_SIZE;
		}
	}
}
