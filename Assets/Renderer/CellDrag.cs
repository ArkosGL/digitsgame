﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

using Digits.Source;


namespace Digits.Renderer
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(CellRenderer), typeof(CellPositioning))]
	[RequireComponent(typeof(CanvasRenderer), typeof(Image))]
	public class CellDrag : MonoBehaviour, IDragHandler, IEndDragHandler
	{
		[SerializeField]
		private RectTransform _rtf;
		[SerializeField]
		private CellRenderer _renderer;
		[SerializeField]
		private CellPositioning _position;

		[SerializeField]
		private bool _drag = false;


#if UNITY_EDITOR
		private void Start()
		{
			if (_rtf == null)
				_rtf = GetComponent<RectTransform>();
			if (_renderer == null)
				_renderer = GetComponent<CellRenderer>();
			if (_position == null)
				_position = GetComponent<CellPositioning>();

			GetComponent<Image>().color = new Color(0f, 0f, 0f, 0f);
			_rtf.sizeDelta = Vector2.one * Constants.CELL_SIZE;
		}
#endif


		public void OnDrag(PointerEventData eventData)
		{
			_rtf.position = eventData.position;
		}

		public void OnEndDrag(PointerEventData eventData)
		{
			var raycast = new List<RaycastResult>();
			GameRenderer.Raycaster.Raycast(eventData, raycast);

			CellRenderer rndr;
			foreach (var result in raycast)
			{
				rndr = result.gameObject.GetComponent<CellRenderer>();
				if (rndr != null && rndr != _renderer && rndr.Replaceable)
				{
					var pos = rndr.GetComponent<CellPositioning>();
					GameController.DoMove(_position.X, pos.X, pos.Y);

					// animations

					//rndr.Init(GameController.Cells[pos.X, pos.Y], pos.X, pos.Y);
					_renderer.Init(GameController.Deck[_position.X], _position.X, 0, true);

					GameRenderer.FieldUpdate();

					return;
				}
			}

			_rtf.localPosition = _position.Position;
		}
	}
}
