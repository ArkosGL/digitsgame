﻿namespace Digits.Renderer
{
	public static class Constants
	{
		public static float CELL_SIZE = 120f;
		public static string BEST_SCORE = "BestScore";
	}
}
