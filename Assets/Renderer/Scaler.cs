﻿using UnityEngine;
using UnityEngine.UI;


[ExecuteInEditMode]
[RequireComponent(typeof(CanvasScaler))]
public class Scaler : MonoBehaviour
{
	[SerializeField]
	private Camera _mainCamera;
	[SerializeField]
	private CanvasScaler _scaler;


	void Start()
	{
		_mainCamera = Camera.main;
		_scaler = GetComponent<CanvasScaler>();
	}


	void Update()
	{
		var w = _mainCamera.pixelWidth / 1080f * 2f;
		var h = _mainCamera.pixelHeight / 1920f * 2f;

		_scaler.scaleFactor = Mathf.Min(w, h);
	}
}
