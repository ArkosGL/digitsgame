﻿using System;
using System.Collections.Generic;


namespace Digits.Source
{
	public class Cell
	{
		public bool Empty = true;
		public CellType Type { get; private set; }
		public int Power { get; private set; }
		public HashSet<CellSide> Spikes { get; private set; }
		public HashSet<CellSide> Walls { get; private set; }


		private Cell(CellType type, int power = 0, HashSet<CellSide> spikes = null, HashSet<CellSide> walls = null)
		{
			Type = type;
			Power = power;

			Spikes = spikes;
			if (Spikes == null)
				Spikes = new HashSet<CellSide>();

			Walls = walls;
			if (Walls == null)
				Walls = new HashSet<CellSide>();
		}


		public void Replace(Cell cell)
		{
			this.Empty = cell.Empty;
			this.Type = cell.Type;
			this.Power = cell.Power;
			this.Spikes = cell.Spikes;
			this.Walls = cell.Walls;
		}


		public void Add(Cell cell)
		{
			if (this.Type != CellType.Hypno)
				this.Power += cell.Power;
			this.Type = cell.Type;
		}


		public static Cell NewEmpty()
		{
			return new Cell(CellType.Empty);
		}


		public static Cell Random()
		{
			var type = (CellType)GameController.Random.Next((int)CellType.count - 2) + 2;

			if (type != CellType.Hypno)
			{
				var power = GameController.Random.Next(Constants.MIN_INITIAL_POWER, Constants.MAX_INITIAL_POWER + 1);


				var spikes = new HashSet<CellSide>();
				for (CellSide side = CellSide.none + 1; side < CellSide.count; side++)
				{
					if (GameController.Random.Next(2) > 0)
						spikes.Add(side);
				}

				var walls = new HashSet<CellSide>();
				for (CellSide side = CellSide.none + 1; side < CellSide.count; side++)
				{
					if (GameController.Random.Next(2) > 0)
						walls.Add(side);
				}

				return new Cell(type, power, spikes, walls);
			}

			return new Cell(type);
		}


		public void HitFrom(Cell cell, CellSide side)
		{
			switch (Type)
			{
				case CellType.Empty:
					return;

				case CellType.Orange:
				case CellType.Green:
				case CellType.Blue:
				case CellType.Purple:
				case CellType.Hypno:
					break;

				case CellType.none:
				case CellType.count:
				default:
					throw new NotImplementedException();
			}


			CellSide wall;
			switch (side)
			{
				case CellSide.Uppr:
					wall = CellSide.Bttm;
					break;
				case CellSide.Rght:
					wall = CellSide.Left;
					break;
				case CellSide.Bttm:
					wall = CellSide.Uppr;
					break;
				case CellSide.Left:
					wall = CellSide.Rght;
					break;
				case CellSide.none:
				case CellSide.count:
				default:
					throw new NotImplementedException();
			}


			if (Walls.Contains(wall))
			{
				Walls.Remove(wall);
			}
			else
			{
				Add(cell);
			}
		}
	}
}
