﻿namespace Digits.Source
{
	public static class Constants
	{
		public static int MIN_INITIAL_POWER = 2;
		public static int MAX_INITIAL_POWER = 5;
		public static int DELTA_INITIAL_POWER = MAX_INITIAL_POWER - MIN_INITIAL_POWER;

		public static int FIELD_SIZE_X = 4;
		public static int FIELD_SIZE_Y = 5;

		public static int DECK_SIZE = FIELD_SIZE_X;
	}
}
