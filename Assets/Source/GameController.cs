﻿using System;
using System.Collections.Generic;


namespace Digits.Source
{
	public static class GameController
	{
		public static Random Random;

		public static Cell[,] Cells;
		public static Cell[] Deck;


		public static void Init()
		{
			Random = new Random();

			Cells = new Cell[Constants.FIELD_SIZE_X, Constants.FIELD_SIZE_Y];
			for (int x = 0; x < Constants.FIELD_SIZE_X; x++)
			{
				for (int y = 0; y < Constants.FIELD_SIZE_Y; y++)
				{
					Cells[x, y] = Cell.NewEmpty();
				}
			}

			Deck = new Cell[Constants.DECK_SIZE];
			for (int x = 0; x < Constants.DECK_SIZE; x++)
			{
				Deck[x] = Cell.Random();
			}
		}


		public static void DoMove(int deck, int x, int y)
		{
			Cells[x, y].Replace(Deck[deck]);
			Deck[deck].Replace(Cell.Random());

			var cell = Cells[x, y];

			Cell hypno = null;

			foreach (var spike in cell.Spikes)
			{
				var target = GetCell(spike, x, y);
				if (target != null)
				{
					if (target.Type == CellType.Hypno)
						hypno = target;
					target.HitFrom(cell, spike);
				}
			}

			if (hypno != null)
			{
				var collapseList = new List<Cell>();
				CollapseScan(ref collapseList, cell.Type, x, y);
				collapseList.Remove(hypno);
				foreach (var cellToCollapse in collapseList)
				{
					hypno.Add(cellToCollapse);
					cellToCollapse.Replace(Cell.NewEmpty());
				}
			}
		}


		private static Cell GetCell(CellSide side, int x, int y)
		{
			switch (side)
			{
				case CellSide.Uppr:
					if (y < Constants.FIELD_SIZE_Y - 1)
						return Cells[x, y + 1];
					break;

				case CellSide.Rght:
					if (x < Constants.FIELD_SIZE_X - 1)
						return Cells[x + 1, y];
					break;

				case CellSide.Bttm:
					if (y > 0)
						return Cells[x, y - 1];
					break;

				case CellSide.Left:
					if (x > 0)
						return Cells[x - 1, y];
					break;

				case CellSide.none:
				case CellSide.count:
				default:
					throw new NotImplementedException();
			}
			return null;
		}


		private static void CollapseScan(ref List<Cell> collapseList, CellType type, int x, int y)
		{
			var cell = Cells[x, y];
			if (cell.Type == type)
			{
				collapseList.Add(cell);

				for (CellSide side = CellSide.none + 1; side < CellSide.count; side++)
				{
					cell = GetCell(side, x, y);
					if (cell != null && cell.Type == type && !collapseList.Contains(cell))
						CollapseScan(ref collapseList, type,
							x + (side == CellSide.Left ? -1 : (side == CellSide.Rght ? 1 : 0)),
							y + (side == CellSide.Bttm ? -1 : (side == CellSide.Uppr ? 1 : 0)));
				}
			}
		}
	}
}
