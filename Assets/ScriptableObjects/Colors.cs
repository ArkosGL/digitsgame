﻿using System;
using System.Linq;
using UnityEngine;

using Digits.Source;


namespace Digits.Renderer
{
	[CreateAssetMenu(fileName = "Colors", menuName = "Digits/Colors")]
	public class Colors : ScriptableObject
	{
		[SerializeField]
		private PairWrap[] _colors;


		public Color Color(CellType type)
		{
			var pair = _colors.FirstOrDefault(p => p.Type == type);

			if (pair == null)
				return UnityEngine.Color.black;

			return pair.Color;
		}


		[Serializable]
		private class PairWrap
		{
			public CellType Type;
			public Color Color;
		}
	}
}
